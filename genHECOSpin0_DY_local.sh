#!/bin/bash
#================================================================================
# THIS SCRIPT PRODUCES GENERATOR LEVEL DY HECO SAMPLES
# Changes must be made to adress of workspace IF NECCESARY.
#================================================================================
echo "User name $USER"
INITIAL="$(echo $USER | head -c 1)"
echo "$INITIAL"
RELEASE=19.2.5.5
TESTAREA=/afs/cern.ch/work/$INITIAL/$USER/DYworkspace
TEMPAREA=/tmp/$USER
OUTAREA=/afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MC/HECO/Spin0

EVTS=50000
ENERGY=13000

cd $TESTAREA/$RELEASE

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
source $AtlasSetup/scripts/asetup.sh $RELEASE,AtlasProduction,here

export MADPATH=/afs/cern.ch/work/$INITIAL/$USER/MG5_aMC_v2_5_5

j=0
RUN0=308382
for charge in 8 10 20 40 60 80
do
for mass in 200 500 1000 1500 2000 2500 3000 4000 5000 6000
do

((j++))

RUN=`expr $RUN0 + $j`

mkdir -p $TEMPAREA/Gen_$RUN 
cd $TEMPAREA/Gen_$RUN 

FILEBASE=MGPy8EG_A14N23LO_DYHECO_
JOPTIONS="MC15."$RUN"."$FILEBASE$charge"e_"$mass"GeV.py"

echo "JOPTIONS=" $JOPTIONS 

OUTFILE="mc15_13TeV."$RUN"."$FILEBASE"spin0.v1.EVNT.pool.root"
OUTLHE="mc15_13TeV."$RUN"."$FILEBASE"spin0.v1.lhe"
LOGFILE="log."$RUN".generate"

cp $TESTAREA/$RELEASE/$JOPTIONS .
cp $TESTAREA/$RELEASE/MadGraphControl_MGPy8EG_A14N23LO_DYHECO.py . 

Generate_tf.py --firstEvent 0 --maxEvents $EVTS --runNumber $RUN --randomSeed $RANDOM --ecmEnergy $ENERGY --jobConfig $JOPTIONS --outputEVNTFile $OUTFILE

mv log.generate $LOGFILE 
mv events.lhe $OUTLHE 

cp $OUTFILE $OUTAREA/$OUTFILE 
cp $OUTLHE $OUTAREA/$OUTLHE
cp $LOGFILE $OUTAREA/$LOGFILE

cd .. 

done
done