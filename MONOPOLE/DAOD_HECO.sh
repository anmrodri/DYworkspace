#!/bin/bash                                                                                                                                                    
#================================================================================                                                                              
# THIS SCRIPT RUNS RECO_TF FROM THE GENERATOR LEVEL FILES HECO DY FILES                                                                                    
# Changes must be made to adress of workspace IF NECCESARY.                                                                                                    
#================================================================================                                                                               

INITIAL="$(echo $USER | head -c 1)"
date
hostname
echo "CONVERTING TO xAOD"
j=0
RUN0=308383

for charge in 8 10 20 40 60 80
do
for mass in 200 500 1000 1500 2000 2500 3000 4000 5000 6000
do

((j++))
echo "I will run file number: "$j

echo "this file has charge" $charge "and mass" $mass
RUN=`expr $RUN0 + $j`
export tmpDir=`mktemp -d`
cd  $tmpDir

echo 'we are in '${PWD}

RUN=`expr $RUN0 + $j`

FILEBASE=MGPy8EG_A14N23LO_DYHECO_
JOPTIONS="MC15."$RUN"."$FILEBASE$charge"e_"$mass"GeV.py"
echo "Working on run:" $RUN
echo "JOPTIONS=" $JOPTIONS

INPUT="mc15_13TeV."$RUN"."$FILEBASE"spin0.v1.EVNT.pool.root"
INDIR=/afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MC/HECO/Spin0

cp $INDIR/$INPUT .
ls
RELEASE='20.1.8.3,AtlasDerivation,here'

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup $RELEASE

OUTPUT=MonopolePG.${RUN}

echo 'Will do reco_tf now, this might take a while.'
Reco_tf.py --inputEVNTFile $INPUT --outputDAODFile=$OUTPUT.DAOD.pool.root --maxEvents=-1 --reductionConf TRUTH0 > Monopole_DAOD_${RUN}.log
echo 'done with reco tf'

cp Monopole_DAOD_${RUN}.log /afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MC/HECO/Spin0/.
cp log.EVNTtoDAOD /afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MC/HECO/Spin0/log.EVNTtoHIT_${RUN}
cp DAOD_TRUTH0.MonopolePG.${RUN}.DAOD.pool.root /afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MC/HECO/Spin0/.

echo "Everything will be saved in MC/HECO/Spin0"

ls -lh

date
echo 'done, now clean up'

cd
#rm -rf DAOD
rm -rf $tmpDir
rm $INDIR/$INPUT
date

done
done
