#!/bin/bash
#================================================================================                                                                    
# THIS SCRIPT RUNS RECO_TF FROM THE GENERATOR LEVEL FILES MONOPOLE DY FILES
# Changes must be made to adress of workspace IF NECCESARY.
#================================================================================           

INITIAL="$(echo $USER | head -c 1)"
date
hostname
echo "CONVERTING TO xAOD"
j=0
RUN0=305495

for charge in 1 2 3
do
for mass in 200 500 1000 1500 2000 2500 3000 4000 5000 6000
do

((j++))
echo "I will run file number: "$j
echo "this file has charge" $charge "and mass" $mass
RUN=`expr $RUN0 + $j`
export tmpDir=`mktemp -d`
cd  $tmpDir

echo 'we are in '${PWD}

RUN=`expr $RUN0 + $j`

FILEBASE=MGPy8EG_A14N23LO_DYMonopole_
JOPTIONS="MC15."$RUN"."$FILEBASE$charge"gD_"$mass"GeV.py"
echo "Working on run:" $RUN
echo "JOPTIONS=" $JOPTIONS

INPUT="mc15_13TeV."$RUN"."$FILEBASE"spin0.v1.EVNT.pool.root"
INDIR=/afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MC/monopole/Spin0

cp $INDIR/$INPUT .
ls
RELEASE='20.1.8.3,AtlasDerivation,here'

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup $RELEASE

OUTPUT=MonopolePG.${RUN}

echo 'Will do Reco_tf now. This will take a while.'
Reco_tf.py --inputEVNTFile $INPUT --outputDAODFile=$OUTPUT.DAOD.pool.root --maxEvents=-1 --reductionConf TRUTH0 > Monopole_DAOD_${RUN}.log
echo 'Done with Reco_tf.'

cp Monopole_DAOD_${RUN}.log /afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MC/monopole/Spin0/.
cp log.EVNTtoDAOD /afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MC/monopole/Spin0/log.EVNTtoHIT_${RUN}
cp DAOD_TRUTH0.MonopolePG.${RUN}.DAOD.pool.root /afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MC/monopole/Spin0/.

echo "Everything will be saved in MC/monopole/Spin0"

ls -lh

date
echo 'done, now clean up'

cd
rm -rf $tmpDir
rm $INDIR/$INPUT
date
done
done
