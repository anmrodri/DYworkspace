#!/bin/bash
#================================================================================                                                                               
# THIS SCRIPT MAKES THE NTUPLES FROM TRUTH DY MONOPOLE FILES                              
# Changes must be made to adress of workspace IF NECCESARY.                                                                                                    
#================================================================================                                                                              
INITIAL="$(echo $USER | head -c 1)"
date
hostname

RUN0=305495
j=0

for charge in 1 2 3
do
for mass in 200 500 1000 1500 2000 2500 3000 4000 5000 6000
do
((j++))
JOB_NUMBER=`expr $RUN0 + $j`

DATA_DIR='/afs/cern.ch/work/'$INITIAL'/'$USER'/DYworkspace/MC/monopole/Spin0'
FILES2COPY='DAOD_TRUTH0.MonopolePG.'$JOB_NUMBER'.DAOD.pool.root'

export tmpDir=`mktemp -d`
cd  $tmpDir
echo 'we are in '${PWD}

cp $DATA_DIR/$FILES2COPY .

cp /afs/cern.ch/work/$INITIAL/$USER/DYworkspace/MONOPOLE/TransientTree/hecoTruth.py .

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
rcSetup -u
rcSetup Base,2.0.19

INPUT=$FILES2COPY

python hecoTruth.py --input $DATA_DIR/$INPUT > tree_$JOB_NUMBER.log

cp tree_$JOB_NUMBER.log $DATA_DIR/
cp hist.root $DATA_DIR/hist_$JOB_NUMBER"_zero".root

ls -lh 

date
echo 'done, now clean up'

cd
rm -rf $tmpDir
rm $DATA_DIR/$INPUT
date
done
done