#!/usr/bin/env python

import sys, os, time, glob, re
import argparse
from ROOT import *
import ROOT

parser = argparse.ArgumentParser()
parser.add_argument('--input', action='store', default="input.txt")
parser.add_argument('--output', action='store', default="hist.root")
args=parser.parse_args()

# Set up ROOT and RootCore:
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )
    
# Initialize the xAOD infrastructure
if(not ROOT.xAOD.Init().isSuccess()): 
    print "Failed xAOD.Init()"
    exit

outfile=ROOT.TFile.Open(args.output,"RECREATE")

monopole_px = std.vector('float')()
monopole_py = std.vector('float')()
monopole_pz = std.vector('float')()
monopole_e = std.vector('float')()
monopole_ekin = std.vector('float')()
monopole_pt = std.vector('float')()
monopole_eta = std.vector('float')()
monopole_phi = std.vector('float')()
monopole_charge = std.vector('float')()
monopole_m = std.vector('float')()
monopole_vx = std.vector('float')()
monopole_vy = std.vector('float')()
monopole_gamma = std.vector('float')()
monopole_beta = std.vector('float')()
monopole_pdgId = std.vector('int')()

MonopoleTree=ROOT.TTree("MonopoleTree","Monopole Analysis Tree")
MonopoleTree.Branch("monopole_px", monopole_px)
MonopoleTree.Branch("monopole_py", monopole_py)
MonopoleTree.Branch("monopole_pz", monopole_pz)
MonopoleTree.Branch("monopole_e", monopole_e)
MonopoleTree.Branch("monopole_ekin", monopole_ekin)
MonopoleTree.Branch("monopole_pt", monopole_pt)
MonopoleTree.Branch("monopole_eta", monopole_eta)
MonopoleTree.Branch("monopole_phi", monopole_phi)
MonopoleTree.Branch("monopole_charge", monopole_charge)
MonopoleTree.Branch("monopole_m", monopole_m)
MonopoleTree.Branch("monopole_vx", monopole_vx)
MonopoleTree.Branch("monopole_vy", monopole_vy)
MonopoleTree.Branch("monopole_gamma", monopole_gamma)
MonopoleTree.Branch("monopole_beta", monopole_beta)
MonopoleTree.Branch("monopole_pdgId", monopole_pdgId)


outputhists=[]
#hnl_c_pt = ROOT.TH1F("hnl_c_pt", "p_{T} of HNL children (charged)", 100, 0, 100)
#outputhists.append(hnl_c_pt)

treeName = "CollectionTree" # default when making transient tree anyway

if ".txt" in args.input:
    inROOTfiles=([file for file in open(args.input).read().strip().split(',')])
else:
    inROOTfiles=(args.input)

# Loop on the input files.  There are surely ways to chain the trees together,
# which is left as an exercise to the reader.
for i in [inROOTfiles]:
    print "Opening file "+i
    f = ROOT.TFile.Open(i)

    if not f:
        print "Couldn't open input file "+i+"...  will not continue."
        sys.exit(1)
        pass

    # Make the "transient tree":
    t = ROOT.xAOD.MakeTransientTree( f, treeName)

    # Print some information:
    print( "Number of input events: %s" % t.GetEntries() )
    for entry in xrange(0,t.GetEntries()):
        if entry%100 == 0: print "Processing event ", entry
        t.GetEntry( entry )

        monopole_px.clear()
        monopole_py.clear()
        monopole_pz.clear()
        monopole_e.clear()
        monopole_ekin.clear()
        monopole_pt.clear()
        monopole_eta.clear()
        monopole_phi.clear()
        monopole_charge.clear()
        monopole_m.clear()
        monopole_vx.clear()
        monopole_vy.clear()
        monopole_gamma.clear()
        monopole_beta.clear()
        monopole_pdgId.clear()
        
        for i in xrange(t.TruthParticles.size()):
            itr=t.TruthParticles.at(i)
            pdgId = itr.pdgId()
            status = itr.status()
            mass = itr.m()/1000
            if(mass>=200 and status == 1): 
                

		gamma=itr.e()/itr.m()
                beta=sqrt(itr.e()*itr.e()-itr.m()*itr.m())/itr.e()
                monopole_px.push_back(itr.px())
                monopole_py.push_back(itr.py())
                monopole_pz.push_back(itr.pz())
                monopole_e.push_back(itr.e())
                monopole_ekin.push_back(itr.e()-itr.m())
                monopole_pt.push_back(itr.pt())
                monopole_eta.push_back(itr.eta())
                monopole_phi.push_back(itr.phi())
                monopole_charge.push_back(itr.charge())
                monopole_m.push_back(itr.m())
                vx = -9999
                vy = -9999
                if itr.hasProdVtx():
                    vx = itr.prodVtx().x()
                    vy = itr.prodVtx().y()
                    pass
                monopole_vx.push_back(vx)
                monopole_vy.push_back(vy)
                monopole_gamma.push_back(gamma)
                monopole_beta.push_back(beta)
                monopole_pdgId.push_back(itr.pdgId())
                pass
            pass

        MonopoleTree.Fill()

        pass # end loop over entries
    f.Close()
    pass

outfile.cd()
for i in outputhists:
    i.Write()
MonopoleTree.Write()
outfile.Close()

sys.exit(0)
